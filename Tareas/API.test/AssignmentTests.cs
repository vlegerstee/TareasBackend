﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.test
{
    public class AssignmentTests
    {
        [Test]
        public void IfVolunteerWhenAcceptInvitationCreateAssignment()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerWhenHaveAssignmentsGetReturnsFilledListOfAssignments()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerWhenHaveNoAssignmentsGetReturnsEmptyListOfAssignments()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerWhenGettingAssignmentsBetweenTwoDatesReturnsOnlyAssignmentsBetweenDates()
        {
            Assert.Fail();
        }
        
        [Test]
        public void IfVolunteerWhenGettingAssignmentWithIdOnlyReturnsSpecificAssignment()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerWhenCancelAssignmentInTimeInvitationReactivatesAndAssignmentIsArchived()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerWhenSearchAssignmentNotAssignedToVolunteerReturnsEmpty()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerGetAssignmentNotAssignedToVolunteerReturnsError()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerWhenCancelAssignmentTooLateCancelationRejected()
        {
            Assert.Fail();
        }

        [Test]
        public void IfAdminWhenWhenCancelAssignmentInTimeInvitationReactivatesAndAssignmentIsArchived()
        {
            Assert.Fail();
        }

        [Test]
        public void IfAdminWhenWhenCancelAssignmentTooLateInvitationReactivatesAndAssignmentIsArchived()
        {
            Assert.Fail();
        }
    }
}
