﻿using Data.Organisational;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace API.test.Builders
{
    public class ClaimsPrincipalBuilder
    {
        public ClaimsPrincipal Build(Member user)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Role, user.Role.Name),
                new Claim("name", user.UserName),
            };

            var identity = new ClaimsIdentity(claims, "Test");
            return new ClaimsPrincipal(identity);

            //var mockPrincipal = new Mock<IPrincipal>();
            //mockPrincipal.Setup(x => x.Identity).Returns(identity);
            //mockPrincipal.Setup(x => x.IsInRole(It.IsAny<string>())).Returns(true);

            //var mockHttpContext = new Mock<HttpContext>();
            //mockHttpContext.Setup(m => m.User).Returns(claimsPrincipal);
        }
    }
}
