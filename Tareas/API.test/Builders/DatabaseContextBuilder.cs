﻿using Data.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.test.Builders
{
    public class DatabaseContextBuilder
    {
        public DbContextOptions<ApplicationDbContext> BuildOptions()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;

            return options;
        }

        public ApplicationDbContext Build()
        {
            var options = BuildOptions();
            return new ApplicationDbContext(options);
        }
    }
}