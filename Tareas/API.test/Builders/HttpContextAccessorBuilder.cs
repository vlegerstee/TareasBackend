﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.test.Builders
{
    public class HttpContextAccessorBuilder
    {
        public HttpContextAccessor Build()
        {
            var httpContext = new DefaultHttpContext();
            var httpContextAccessor = new HttpContextAccessor
            {
                HttpContext = httpContext
            };

            return httpContextAccessor;
        }
    }
}