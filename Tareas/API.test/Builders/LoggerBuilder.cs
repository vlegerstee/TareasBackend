﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.test.Builders
{
    public class LoggerBuilder
    {
        private readonly ServiceProvider serviceProvider;

        public LoggerBuilder()
        {
            serviceProvider = new ServiceCollection()
             .AddLogging()
             .BuildServiceProvider();
        }

        public ILogger<T> Build<T>()
        {
            var factory = serviceProvider.GetService<ILoggerFactory>();
            return factory.CreateLogger<T>();
        }
    }
}
