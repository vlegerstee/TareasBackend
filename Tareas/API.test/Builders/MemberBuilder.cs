﻿using Data.Organisational;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace API.test.Builders
{
    public class MemberBuilder
    {
        public Member BuildAdmin()
        {
            var member = new Member
            {
                UserName = "Mister Admin",
                FirstName = "Mister",
                LastName = "Admin",
                Role = new MemberRole
                {
                    Name = MemberRoles.Admin
                }
            };

            return member;
        }

        public Member BuildVolunteer()
        {
            var member = new Member
            {
                UserName = "Miss Volunteer",
                FirstName = "Miss",
                LastName = "Volunteer",
                Role = new MemberRole
                {
                    Name = MemberRoles.Volunteer
                }
            };

            return member;
        }
    }
}
