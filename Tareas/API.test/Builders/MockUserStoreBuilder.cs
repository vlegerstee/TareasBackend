﻿using Data.Organisational;
using Microsoft.AspNetCore.Identity;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.test.Builders
{
    public class MockUserStoreBuilder
    {
        public UserManager<Member> Build()
        {
            var mockUserStore = new Mock<IUserStore<Member>>();
            var userManager = new UserManager<Member>(mockUserStore.Object, null, null, null, null, null, null, null, null);
            return userManager;
        }
    }
}
