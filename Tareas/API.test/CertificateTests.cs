﻿using API.Controllers;
using API.Services;
using API.test.Builders;
using API.ViewModels;
using Data.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using NUnit;
using System;

namespace API.test
{
    public class CertificateTests
    {
        private CertificateController certificateController;
        private CertificateService certificateService;
        private ApplicationDbContext databaseContext;
        private HttpContextAccessor httpContextAccessor;

        [SetUp]
        public void Setup()
        {
            var loggerBuilder = new LoggerBuilder();
            var userManager = new MockUserStoreBuilder().Build();
            httpContextAccessor = new HttpContextAccessorBuilder().Build();
            var databaseContext = new DatabaseContextBuilder().Build();
            certificateService = new CertificateService(userManager, httpContextAccessor, databaseContext);
            certificateController = new CertificateController(userManager, certificateService, loggerBuilder.Build<CertificateController>());
        }

        [Test]
        public void IfCreateCertificateAsVolunteerThenFailsWithException()
        {
            var volunteer = new MemberBuilder().BuildVolunteer();
            var claimsPrincipal = new ClaimsPrincipalBuilder().Build(volunteer);
            httpContextAccessor.HttpContext.User = claimsPrincipal;

            var certificateViewModel = new CertificateViewModel();
            certificateViewModel.Name = "TestCertificate";
            Assert.ThrowsAsync<UnauthorizedAccessException>(() => certificateController.PostCertificate(certificateViewModel));
        }

        [Test]
        public void IfCreateCertificateAsAdminThenSucceeds()
        {
            var admin = new MemberBuilder().BuildAdmin();
            var claimsPrincipal = new ClaimsPrincipalBuilder().Build(admin);
            httpContextAccessor.HttpContext.User = claimsPrincipal;

            var certificateViewModel = new CertificateViewModel();
            certificateViewModel.Name = "TestCertificate";
            var result = (CertificateViewModel)((CreatedAtActionResult)certificateController.PostCertificate(certificateViewModel).Result).Value;
            var actual = certificateController.GetCertificate(result.Id).Result;
        }

        [Test]
        public void GetAllCertificatesAsVolunteerThenGetsAllCertificates()
        {
            //var filter = new FilterViewModel();
            //var result = certificateController.GetList(filterModel).Result;
            Assert.Fail();
        }

        [Test]
        public void GetAllCertificatesAsAdminThenGetsAllCertificates()
        {
            //var filter = new FilterViewModel();
            //var result = certificateController.GetList(filterModel).Result;
            Assert.Fail();
        }

        [Test]
        public void GetSpecificCertificateAsVolunteerThenGetsCertificate()
        {
            Assert.Fail();
        }

        [Test]
        public void GetSpecificCertificateAsAdminThenGetsCertificate()
        {
            Assert.Fail();
        }
        
        [Test]
        public void GetSpecificCertificateThatDoesntExistsReturnsNull()
        {
            Assert.Fail();
        }

        [Test]
        public void DeleteCertificateAsVolunteerThenFailsWithAuthorizationException()
        {
            Assert.Fail();
        }

        [Test]
        public void DeleteCertificateAsAdminThenCertificateSuccessfullyRemoved()
        {
            Assert.Fail();
        }

        [Test]
        public void ChangeCertificateNameAsVolunteerThenFailsWithAuthorizationException()
        {
            Assert.Fail();
        }

        [Test]
        public void ChangeCertificateNameAsAdminThenCertificateNameSuccessfullyChanged()
        {
            Assert.Fail();
        }
    }
}
