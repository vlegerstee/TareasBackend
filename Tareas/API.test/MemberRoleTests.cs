﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.test
{
    public class MemberRoleTests
    {
        [Test]
        public void WhenChangeMemberRoleFromVolunteerToAdministratorWithRightsThenMemberIsAdministrator()
        {
            Assert.Fail();
        }

        [Test]
        public void WhenChangeMemberRoleFromVolunteerToAdministratorWithoutRightsThenFail()
        {
            Assert.Fail();
        }

        [Test]
        public void WhenChangeMemberRoleFromAdministratorToVolunteerWithRightsThenMemberIsVolunteer()
        {
            Assert.Fail();
        }

        [Test]
        public void WhenChangeMemberRoleFromAdministratorToVolunteerWithoutRightsThenFail()
        {
            Assert.Fail();
        }
    }
}
