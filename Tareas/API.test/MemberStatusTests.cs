﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.test
{
    public class MemberStatusTests
    {
        [Test]
        public void WhenActivateInactiveMemberThenMemberStatusIsActive()
        {
            Assert.Fail();
        }

        [Test]
        public void WhenBlockInactiveMemberThenFail()
        {
            Assert.Fail();
        }

        [Test]
        public void WhenBlockActiveMemberThenSuccess()
        {
            Assert.Fail();
        }

        [Test]
        public void WhenDeactivateActiveMemberThenMemberStatusIsInactive()
        {
            Assert.Fail();
        }

        [Test]
        public void WhenUnblockBlockedMemberThenMemberStatusIsActive()
        {
            Assert.Fail();
        }
    }
}
