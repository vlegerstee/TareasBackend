﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.test
{
    public class OrganisationTests
    {
        [SetUp]
        public void Setup()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerCreatesOrganisationThenFail()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerDeleteOrganisationThenFail()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerAddMemberThenFail()
        {
            Assert.Fail();
        }

        [Test]
        public void IfVolunteerRemoveMemberThenFail()
        {
            Assert.Fail();
        }

        [Test]
        public void IfAdminCreateOrganisationThenOrganisationIsCreated()
        {
            Assert.Fail();
        }

        [Test]
        public void IfAdminDeleteOrganisationThenOrganisationIsDeleted()
        {
            Assert.Fail();
        }

        [Test]
        public void IfAdminAddMemberThenMemberIsAdded()
        {
            Assert.Fail();
        }

        [Test]
        public void IfAdminRemoveMemberThenMemberIsRemoved()
        {
            Assert.Fail();
        }
    }
}
