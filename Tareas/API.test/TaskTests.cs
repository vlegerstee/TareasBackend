﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.test
{
    public class TaskTests
    {
        [Test]
        public void CreateTaskWithoutRightsThenFail()
        {
            Assert.Fail();
        }

        [Test]
        public void DeleteTaskWithoutRightsThenFail()
        {
            Assert.Fail();
        }

        [Test]
        public void ChangeTaskNameWithoutRightsThenFail()
        {
            Assert.Fail();
        }

        [Test]
        public void CreateTaskWithRightsThenTaskCreated()
        {
            Assert.Fail();
        }

        [Test]
        public void DeleteTaskWithRightsThenTaskDeleted()
        {
            Assert.Fail();
        }

        [Test]
        public void ChangeTaskNameWithRightsThenTaskNameChanged()
        {
            Assert.Fail();
        }
    }
}
