﻿using API.Services.Interfaces;
using Data.Organisational;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    //[Authorize]
    [ProducesResponseType(typeof(List<string>), 400)]
    public class AssignmentsController : BaseController
    {
        private readonly IAssignmentService mAssignmentService;
        private readonly ILogger<AssignmentsController> mLogger;

        public AssignmentsController(UserManager<Member> userManager,
                              IAssignmentService assignmentService,
                              ILogger<AssignmentsController> logger)
            : base(userManager)
        {
            mAssignmentService = assignmentService;
            mLogger = logger;
        }
    }
}
