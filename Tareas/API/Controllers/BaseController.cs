﻿using Data.Organisational;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class BaseController : ControllerBase
    {
        private readonly UserManager<Member> mUserManager;
        private Member mAppUser;
        public Member CurrentUser => GetCurrentUser();

        public BaseController(UserManager<Member> userManager)
        {
            this.mUserManager = userManager;
        }

        private Member GetCurrentUser()
        {
            if (!User.Identity.IsAuthenticated)
                return null;

            if (mAppUser != null)
                return mAppUser;

            var userId = User?.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
            mAppUser = mUserManager.FindByIdAsync(userId).Result;
            return mAppUser;
        }
    }
}
