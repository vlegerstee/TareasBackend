﻿using API.Services.Interfaces;
using API.ViewModels;
using Data.Organisational;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    //[Authorize]
    [ProducesResponseType(typeof(List<string>), 400)]
    public class CertificateController : BaseController
    {
        private readonly ICertificateService mCertificateService;
        private readonly ILogger<CertificateController> mLogger;

        public CertificateController(UserManager<Member> userManager,
                              ICertificateService certificateService,
                              ILogger<CertificateController> logger)
            : base(userManager)
        {
            mCertificateService = certificateService;
            mLogger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(FilterViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var certificates = await mCertificateService.GetListAsync(model);

            if (!certificates.Succeeded)
                return BadRequest(certificates.Errors);

            return Ok(certificates.Value);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCertificate(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var certificate = await mCertificateService.GetCertificate(id);

            if (certificate == null)
            {
                return NotFound();
            }

            return Ok(certificate);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCertificate(Guid id, [FromBody]CertificateViewModel certificate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != certificate.Id)
            {
                return BadRequest();
            }

            certificate.Id = id;
            certificate = await mCertificateService.UpdateCertificate(certificate);

            if (certificate == null)
            {
                return NotFound();
            }

            return Ok(certificate);
        }

        [HttpPost]
        public async Task<IActionResult> PostCertificate([FromBody]CertificateViewModel certificate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            certificate = await mCertificateService.CreateCertificate(certificate);

            return CreatedAtAction("GetCertificate", new { id = certificate.Id }, certificate);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCertificate(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var certificate = await mCertificateService.DeleteCertificate(id);

            if (certificate == null)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
