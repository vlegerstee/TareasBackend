﻿using API.Services.Interfaces;
using Data.Organisational;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    //[Authorize]
    [ProducesResponseType(typeof(List<string>), 400)]
    public class InvitationController : BaseController
    {
        private readonly IInvitationService mInvitationService;
        private readonly ILogger<InvitationController> mLogger;

        public InvitationController(UserManager<Member> userManager,
                                 IInvitationService invitationService,
                                 ILogger<InvitationController> logger)
               : base(userManager)
        {
            mInvitationService = invitationService;
            mLogger = logger;
        }
    }
}