﻿using API.Services.Interfaces;
using Data.Organisational;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    //[Authorize]
    [ProducesResponseType(typeof(List<string>), 400)]
    public class MemberController : BaseController
    {
        private readonly IMemberService mMemberService;
        private readonly ILogger<MemberController> mLogger;

        public MemberController(UserManager<Member> userManager,
                              IMemberService memberService,
                              ILogger<MemberController> logger)
            : base(userManager)
        {
            mMemberService = memberService;
            mLogger = logger;
        }
    }
}
