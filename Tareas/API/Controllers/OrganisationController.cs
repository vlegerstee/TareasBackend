﻿using API.Services.Interfaces;
using Data.Organisational;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    //[Authorize]
    [ProducesResponseType(typeof(List<string>), 400)]
    public class OrganisationController : BaseController
    {
        private readonly IOrganisationService mOrganisationService;
        private readonly ILogger<OrganisationController> mLogger;

        public OrganisationController(UserManager<Member> userManager,
                              IOrganisationService organisationService,
                              ILogger<OrganisationController> logger)
            : base(userManager)
        {
            mOrganisationService = organisationService;
            mLogger = logger;
        }
    }
}
