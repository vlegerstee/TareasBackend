﻿using API.Services.Interfaces;
using Data.Organisational;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    //[Authorize]
    [ProducesResponseType(typeof(List<string>), 400)]
    public class TaskController : BaseController
    {
        private readonly ITaskService mTaskService;
        private readonly ILogger<TaskController> mLogger;

        public TaskController(UserManager<Member> userManager,
                                 ITaskService taskService,
                                 ILogger<TaskController> logger)
               : base(userManager)
        {
            mTaskService = taskService;
            mLogger = logger;
        }
    }
}