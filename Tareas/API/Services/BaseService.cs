﻿using Data.DAL;
using Data.Organisational;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace API.Services
{
    public class BaseService
    {
        public readonly UserManager<Member> mUserManager;
        private readonly IHttpContextAccessor mContextAccessor;
        protected readonly ApplicationDbContext mContext;
        protected HttpContext mHttpContext { get { return mContextAccessor.HttpContext; } }
        private Member mAppUser;
        public Member CurrentUser => GetCurrentUser();
        

        public BaseService(UserManager<Member> userManager,
                           IHttpContextAccessor contextAccessor,
                           ApplicationDbContext context)
        {
            mUserManager = userManager;
            mContextAccessor = contextAccessor;
            mContext = context;
        }

        private Member GetCurrentUser()
        {
            if (!mHttpContext.User.Identity.IsAuthenticated)
                return new Member();

            if (mAppUser != null)
                return mAppUser;

            var userId = mHttpContext.User?.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
            mAppUser = mUserManager.FindByIdAsync(userId).Result;
            return mAppUser;
        }


        protected (bool isCreated, TResult result) GetOrCreateEntity<TResult>(
            DbSet<TResult> sourceCollection,
            Expression<Func<TResult, bool>> whereConditions = null)
            where TResult : class, new()
        {
            var isCreated = false;
            TResult result = null;

            if (whereConditions != null)
                result = sourceCollection.FirstOrDefault(whereConditions);

            if (result == null)
            {
                isCreated = true;
                result = new TResult();
                sourceCollection.Add(result);
            }

            var creatorId = typeof(TResult).GetProperty("CreatorId");
            if (creatorId != null && isCreated)
                creatorId.SetValue(result, CurrentUser.Id);

            var modifierId = typeof(TResult).GetProperty("ModifierId");
            if (modifierId != null)
                modifierId.SetValue(result, CurrentUser.Id);

            var createDate = typeof(TResult).GetProperty("CreateDate");
            if (createDate != null && isCreated)
                createDate.SetValue(result, DateTime.UtcNow);

            var modifyDate = typeof(TResult).GetProperty("ModifyDate");
            if (modifyDate != null)
                modifyDate.SetValue(result, DateTime.UtcNow);

            var hversion = typeof(TResult).GetProperty("HVersion");
            if (hversion != null)
                hversion.SetValue(result, (int)hversion.GetValue(result) + 1);

            return (isCreated, result);
        }

        protected async Task<(bool isCreated, TResult result)> GetOrCreateEntityAsync<TResult>(
            DbSet<TResult> sourceCollection,
            Expression<Func<TResult, bool>> whereConditions = null)
            where TResult : class, new()
        {
            var isCreated = false;
            TResult result = null;

            if (whereConditions != null)
                result = await sourceCollection.FirstOrDefaultAsync(whereConditions);

            if (result == null)
            {
                isCreated = true;
                result = new TResult();
                await sourceCollection.AddAsync(result);
            }

            var creatorId = typeof(TResult).GetProperty("CreatorId");
            if (creatorId != null && isCreated)
                creatorId.SetValue(result, CurrentUser.Id);

            var modifierId = typeof(TResult).GetProperty("ModifierId");
            if (modifierId != null)
                modifierId.SetValue(result, CurrentUser.Id);

            var createDate = typeof(TResult).GetProperty("CreateDate");
            if (createDate != null && isCreated)
                createDate.SetValue(result, DateTime.UtcNow);

            var modifyDate = typeof(TResult).GetProperty("ModifyDate");
            if (modifyDate != null)
                modifyDate.SetValue(result, DateTime.UtcNow);

            var hversion = typeof(TResult).GetProperty("HVersion");
            if (hversion != null)
                hversion.SetValue(result, (int)hversion.GetValue(result) + 1);

            return (isCreated, result);
        }
    }
}