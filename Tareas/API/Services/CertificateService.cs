﻿using API.Models;
using API.Services.Interfaces;
using API.ViewModels;
using Data.DAL;
using Microsoft.EntityFrameworkCore;
using Data.Organisational;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services
{
    public class CertificateService : BaseService, ICertificateService
    {
        public CertificateService(UserManager<Member> userManager,
                                IHttpContextAccessor contextAccessor,
                                ApplicationDbContext context)
              : base(userManager, contextAccessor, context)
        {
        }

        [Authorize(Roles = MemberRoles.Admin + "," + MemberRoles.Volunteer)]
        public async Task<ProcessResult<List<CertificateViewModel>>> GetListAsync(FilterViewModel model)
        {
            return await Process.RunAsync(async () =>
            {
                var takeSize = model.TakeSize;
                var certificateList = await mContext.Certificates.Include(x => x.Name)
                                            .Take(takeSize)
                                            .Select(x => new CertificateViewModel
                                            {
                                                Id = x.Id,
                                                Name = x.Name,
                                            }).ToListAsync();

                return certificateList;
            });
        }

        [Authorize(Roles = MemberRoles.Admin + "," + MemberRoles.Volunteer)]
        public async Task<CertificateViewModel> GetCertificate(Guid id)
        {
            if (mHttpContext.User.IsInRole(MemberRoles.Admin))
            {
                var certificate = await mContext.Certificates.FindAsync(id);

                if (certificate == null)
                {
                    return null;
                }

                return certificate.ToCertificateViewModel();
            }
            else if (mHttpContext.User.IsInRole(MemberRoles.Volunteer))
            {
                var certificate = await mContext.Certificates.SingleOrDefaultAsync(x => x.Id == id && x.CreatorId == CurrentUser.Id);

                if (certificate == null)
                {
                    return null;
                }

                return certificate.ToCertificateViewModel();
            }
            else
            {
                throw new InvalidOperationException(ExceptionMessages.MemberRoleUnknown);
            }
        }

        [Authorize(Roles = MemberRoles.Admin)]
        public async Task<CertificateViewModel> UpdateCertificate(CertificateViewModel vm)
        {
            if (!mHttpContext.User.IsInRole(MemberRoles.Admin))
            {
                throw new UnauthorizedAccessException("User not admin");
            }

            var certificate = await mContext.Certificates.FindAsync(vm.Id);

            if (certificate == null)
            {
                return null;
            }

            certificate.Name = vm.Name;
            await mContext.SaveChangesAsync();
            return certificate.ToCertificateViewModel();
        }

        [Authorize(Roles = MemberRoles.Admin)]
        public async Task<CertificateViewModel> CreateCertificate(CertificateViewModel vm)
        {
            if (!mHttpContext.User.IsInRole(MemberRoles.Admin))
            {
                throw new UnauthorizedAccessException("User not admin");
            }

            var tracking = mContext.Certificates.Add(vm.ToCertificateModel());
            await mContext.SaveChangesAsync();
            return tracking.Entity.ToCertificateViewModel();
        }

        [Authorize(Roles = MemberRoles.Admin)]
        public async Task<CertificateViewModel> DeleteCertificate(Guid id)
        {
            if (!mHttpContext.User.IsInRole(MemberRoles.Admin))
            {
                throw new UnauthorizedAccessException("User not admin");
            }

            var certificate = await mContext.Certificates.FindAsync(id);

            if (certificate == null)
            {
                return null;
            }

            mContext.Certificates.Remove(certificate);
            await mContext.SaveChangesAsync();
            return certificate.ToCertificateViewModel();
        }
    }
}
