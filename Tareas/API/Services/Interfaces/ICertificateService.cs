﻿using API.Models;
using API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services.Interfaces
{
    public interface ICertificateService
    {
        Task<ProcessResult<List<CertificateViewModel>>> GetListAsync(FilterViewModel model);
        Task<CertificateViewModel> GetCertificate(Guid id);
        Task<CertificateViewModel> UpdateCertificate(CertificateViewModel certificate);
        Task<CertificateViewModel> CreateCertificate(CertificateViewModel certificate);
        Task<CertificateViewModel> DeleteCertificate(Guid id);
    }
}
