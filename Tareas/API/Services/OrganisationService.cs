﻿using API.Services.Interfaces;
using Data.DAL;
using Data.Organisational;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services
{
    public class OrganisationService : BaseService, IOrganisationService
    {
        public OrganisationService(UserManager<Member> userManager,
                           IHttpContextAccessor contextAccessor,
                           ApplicationDbContext context)
         : base(userManager, contextAccessor, context)
        {
        }
    }
}
