﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public class CertificateViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
