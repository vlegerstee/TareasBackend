﻿using Data.Organisational;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.ViewModels
{
    public static class ViewModelExtensions
    {
        public static CertificateViewModel ToCertificateViewModel(this Certificate certificate)
        {
            return new CertificateViewModel()
            {
                Id = certificate.Id,
                Name = certificate.Name
            };
        }

        public static Certificate ToCertificateModel(this CertificateViewModel certificate)
        {
            return new Certificate()
            {
                Id = certificate.Id,
                Name = certificate.Name
            };
        }
    }
}
