﻿using Data.Organisational;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.DAL
{
    public static class DBSeed
    {
        public static void Seed(ModelBuilder builder)
        {
            builder.Entity<Certificate>().HasData(
               new Certificate
               {
                   Id = Guid.NewGuid(),
                   Name = "Scheidsrechter"
               }
           );
        }
    }
}
