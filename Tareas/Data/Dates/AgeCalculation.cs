﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Dates
{
    public static class AgeCalculation
    {
        public static int CalculateAge(DateTime birthDay)
        {
            DateTime now = DateTime.Today;
            var age = now.Year - birthDay.Year;
            if (birthDay.Date > now.AddYears(-age)) age--;
            return age;
        }
    }
}
