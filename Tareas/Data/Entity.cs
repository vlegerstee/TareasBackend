﻿using Data.Organisational;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data
{
    public class Entity : IEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Member Creator { get; set; }

        [Required]
        public Guid CreatorId { get; set; }

        [Required]
        public Guid ModifierId { get; set; }

        [Required]
        public DateTime CreateDate { get; set; }

        [Required]
        public DateTime ModifyDate { get; set; }

        // HVersion gets +1 whenever a record is updated
        [Required]
        public int HVersion { get; set; }
    }
}
