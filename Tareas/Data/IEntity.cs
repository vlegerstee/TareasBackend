﻿using Data.Organisational;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public interface IEntity
    {
        Guid Id { get; set; }

        Member Creator { get; set; }
        Guid CreatorId { get; set; }

        Guid ModifierId { get; set; }
        DateTime CreateDate { get; set; }
        DateTime ModifyDate { get; set; }
        // HVersion gets +1 whenever a record is updated
        int HVersion { get; set; }
    }
}