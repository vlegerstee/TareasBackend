﻿using Data.Organisational;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Invitations
{
    public class TargetList : Entity
    {
        public List<Member> Members = new List<Member>();
    }
}
