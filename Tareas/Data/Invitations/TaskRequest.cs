﻿using Data.Tasks;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Invitations
{
    /// <summary>
    /// Sent to volunteers to request if they can pick up a task
    /// </summary>
    public class TaskRequest : Entity
    {
        public Task Task { get; set; }
        public TargetList Targets = new TargetList();
    }
}
