﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Organisational
{
    public class Certificate : Entity
    {
        [Required]
        public string Name { get; set; }
    }
}