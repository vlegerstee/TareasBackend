﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Organisational
{
    // A certification is a member-specific certificate link, certification can expire.
    public class Certification : Entity
    {
        [Required]
        public Certificate Certificate { get; set; }

        [Required]
        public DateTime ExpirationDate { get; set; }
    }
}
