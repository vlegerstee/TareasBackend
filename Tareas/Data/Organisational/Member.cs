﻿using Data.Dates;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Organisational
{
    public class Member : IdentityUser<Guid>
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public override Guid Id
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public DateTime Birthday { get; set; }

        [Required]
        public MemberRole Role { get; set; }

        [Required]
        public MemberStatus Status { get; set; }
        private MemberStatus PreBlockStatus { get; set; }
        public ICollection<Certification> Certifications { get; set; }
        public TaskPreferences TaskPreferences { get; set; }

        public ICollection<Certificate> Certificates { get; set; }

        public int Age
        {
            get
            {
                return AgeCalculation.CalculateAge(Birthday);
            }
        }

        public void Block()
        {
            PreBlockStatus = Status;
            Status = MemberStatus.Blocked;
        }

        public void UnBlock()
        {
            if(Status != MemberStatus.Blocked)
            {
                return;
            }

            Status = PreBlockStatus;
        }

        public bool IsBlocked { get { return Status == MemberStatus.Blocked; } }
    }
}
