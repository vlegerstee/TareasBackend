﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Organisational
{
    public static class MemberRoles
    {
        public const string Admin = "Admin";
        public const string Volunteer = "Volunteer";
    }
}
