﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Organisational
{
    public enum MemberStatus
    {
        Active,
        Inactive,
        Blocked
    }
}
