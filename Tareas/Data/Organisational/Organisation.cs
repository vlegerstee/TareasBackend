﻿using Data.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Organisational
{
    public class Organisation : Entity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public OrganisationType GetOrganisationType { get; set; }
        public List<Member> Members { get; }

        public void AddMember(Member member)
        {
            Members.Add(member);
        }

        public void RemoveMember(Member member)
        {
            Members.Remove(member);
        }
    }
}
