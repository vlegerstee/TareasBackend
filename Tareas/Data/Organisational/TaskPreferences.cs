﻿using Data.Tasks;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Organisational
{
    public class TaskPreferences : Entity
    {
        /// <summary>
        /// Categories preferred by the user
        /// </summary>
        public List<TaskCategory> Categories { get; set; }
    }
}
