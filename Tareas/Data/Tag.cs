﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data
{
    public class Tag : Entity
    {
        [Required]
        public string Name { get; set; } = string.Empty;
    }
}
