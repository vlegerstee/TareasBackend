﻿using Data.Organisational;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Tasks
{
    public interface ITaskRequirement : IEntity
    {
        /// <summary>
        /// Check if a member meets the task requirements 
        /// </summary>
        bool MeetsRequirement(Member member);
    }
}
