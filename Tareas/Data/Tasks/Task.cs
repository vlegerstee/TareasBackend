﻿using Data.Organisational;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data.Tasks
{
    public class Task : Entity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public Organisation Organisation { get; set; }

        [Required]
        public TaskState State { get; set; }

        /// <summary>
        /// A task falls in a single category (eg. Cleaning, Barduty)
        /// </summary>
        [Required]
        public TaskCategory Category { get; set; }

        /// <summary>
        /// Set of requirements for a member to pick up this task
        /// </summary>
        public TaskRequirementSet Requirement { get; set; }

        /// <summary>
        /// Tasks can have multiple tags (eg. evening, mentor, etc.)
        /// </summary>
        public List<Tag> Tags { get; set; }

        public Timeframe Start { get; set; }
        public Timeframe End { get; set; }
    }
}
