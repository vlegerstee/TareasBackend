﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Data.Organisational;

namespace Data.Tasks
{
    public class TaskAgeRequirement : Entity, ITaskRequirement
    {
        [Required]
        public int MinimumAge { get; set; } = 12;

        [Required]
        public int MaximumAge { get; set; } = 99;

        public bool MeetsRequirement(Member member)
        {
            var age = member.Age;
            return age >= MinimumAge && age <= MaximumAge;
        }
    }
}
