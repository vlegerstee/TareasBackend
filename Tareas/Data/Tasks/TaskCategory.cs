﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Tasks
{
    public class TaskCategory : Entity
    {
        [Required]
        public string Name { get; set; }
    }
}
