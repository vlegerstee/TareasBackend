﻿using Data.Organisational;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Tasks
{
    public class TaskCertificateRequirement : Entity, ITaskRequirement
    {
        public List<Certification> RequiredCertifications { get; set; } = new List<Certification>();

        public bool MeetsRequirement(Member member)
        {
            foreach (var requiredCertification in RequiredCertifications)
            {
                if(!member.Certifications.Contains(requiredCertification))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
