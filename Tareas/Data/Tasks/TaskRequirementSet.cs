﻿using Data.Organisational;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Tasks
{
    public class TaskRequirementSet : Entity, ITaskRequirement
    {
        public List<ITaskRequirement> Requirements = new List<ITaskRequirement>();

        public bool MeetsRequirement(Member member)
        {
            foreach (var requirement in Requirements)
            {
                if(!requirement.MeetsRequirement(member))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
