﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Tasks
{
    public class TaskState : Entity
    {
        [Required]
        public string Name { get; set; } = string.Empty;
    }
}
