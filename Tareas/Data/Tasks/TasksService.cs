﻿using Data.Organisational;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Tasks
{
    public static class TasksService
    {
        public static IEnumerable<Task> GetTasksForOrganisation(Organisation organisation)
        {
            return null;
        }

        public static IEnumerable<Task> GetTasksForOrganisation(Organisation organisation, DateTime startDate)
        {
            return new List<Task>();
        }

        public static IEnumerable<Task> GetTasksForOrganisation(Organisation organisation, DateTime startDate, DateTime endDate)
        {
            return new List<Task>();
        }

        public static IEnumerable<Task> GetTasksForMember(Member member)
        {
            return new List<Task>();
        }

        public static IEnumerable<Task> GetTasksForMember(Member member, DateTime startDate)
        {
            return new List<Task>();
        }

        public static IEnumerable<Task> GetTasksForMember(Member member, DateTime startDate, DateTime endDate)
        {
            return new List<Task>();
        }

        /// <summary>
        /// Finds all members that are eligable to perform the task
        /// </summary>
        public static IEnumerable<Member> FindEligableMembers(this Task task, IEnumerable<Member> members)
        {
            var result = new List<Member>();

            foreach (var member in members)
            {
                if (task.Requirement.MeetsRequirement(member))
                {
                    result.Add(member);
                }
            }

            return result;
        }
    }
}
