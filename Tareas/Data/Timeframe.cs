﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data
{
    public class Timeframe
    {
        [Required]
        public DateTime Date { get; set; }
    }
}
